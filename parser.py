# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup


class Parser:

    def __init__(self, raw):

        self.raw = raw

    def data(self):

        parsed = {
            'accounts': self._accounts(),
            'cards': self._cards(),
        }
        return parsed

    def _accounts(self):

        raw = BeautifulSoup(self.raw["accounts"], "lxml")
        parsed = []
        raw__account__types = raw.find_all("article", id = "print-section")
        for raw__account__type in raw__account__types:
            parsed__account__type = raw__account__type.header.h4.text.strip()
            parsed__accounts = []
            raw__accounts = raw__account__type.find("div", class_="prestamos-resumen").table.find_all("tr")
            for raw__account in raw__accounts:
                try:
                    if raw__account['data-url'] != "":
                        parsed__account__account_number = raw__account.find("td", class_ = "producto").find("span", class_ = "link").text.strip()
                        parsed__account__countable_balance = raw__account.find_all('td')[1].find("strong", class_ = "saldoSinEncriptar").text.strip()
                        parsed__account__available_balance = raw__account.find_all('td')[2].find("strong", class_ = "saldoSinEncriptar").text.strip()
                        if parsed__account__available_balance.startswith("$"):
                            parsed__account__currency = "USD"
                            parsed__account__countable_balance = parsed__account__countable_balance.replace("$", "").strip()
                            parsed__account__available_balance = parsed__account__available_balance.replace("$", "").strip()
                        else:
                            parsed__account__currency = "PEN"
                            parsed__account__countable_balance = parsed__account__countable_balance.replace("S/", "").strip()
                            parsed__account__available_balance = parsed__account__available_balance.replace("S/", "").strip()
                        parsed__account__countable_balance = float(parsed__account__countable_balance.replace(",", ""))
                        parsed__account__available_balance = float(parsed__account__available_balance.replace(",", ""))
                        parsed__account = {
                            "currency": parsed__account__currency,
                            "account_number": parsed__account__account_number,
                            "balance": {
                                "countable": parsed__account__countable_balance,
                                "available": parsed__account__available_balance,
                            }
                        }
                        parsed__accounts.append(parsed__account)
                except KeyError: # first row has titles
                    pass
            parsed.append({
                "type": parsed__account__type,
                "accounts": parsed__accounts,
            })
        return parsed

    def _cards(self):

        raw = BeautifulSoup(self.raw["cards"], "lxml")
        parsed = []
        raw__card__types = raw.find_all("article", id = "print-section")
        for raw__card__type in raw__card__types:
            parsed__card__type = raw__card__type.header.h4.text.strip()
            parsed__cards = []
            raw__cards = raw__card__type.find("table", id = "tbl_tarjetas_resumen").find_all("tr")
            for raw__card in raw__cards:
                try:
                    if raw__card['data-url'] != "":
                        parsed__card__detail = " ".join(raw__card.find_all("td")[0]["class"])
                        parsed__card__number = raw__card.find_all("td")[0].find("span", class_ = "link").text
                        parsed__card__name = raw__card.find_all("td")[0].find("small", class_ = "nombreProducto").text
                        parsed__card__titular = raw__card.find_all("td")[0].find("small", class_ = "estitular T").text
                        parsed__card__is_titular = False
                        if parsed__card__titular == "TITULAR":
                            parsed__card__is_titular = True
                        parsed__card__limit_balance = raw__card.find_all('td')[1].find("strong", class_ = "saldoSinEncriptar").text.strip()
                        parsed__card__available_balance = raw__card.find_all('td')[2].find("strong", class_ = "saldoSinEncriptar").text.strip()
                        if parsed__card__limit_balance.startswith("$"):
                            parsed__card__currency = "USD"
                            parsed__card__limit_balance = parsed__card__limit_balance.replace("$", "").strip()
                            parsed__card__available_balance = parsed__card__available_balance.replace("$", "").strip()
                        else:
                            parsed__card__currency = "PEN"
                            parsed__card__limit_balance = parsed__card__limit_balance.replace("S/", "").strip()
                            parsed__card__available_balance = parsed__card__available_balance.replace("S/", "").strip()
                        parsed__card__limit_balance = float(parsed__card__limit_balance.replace(",", ""))
                        parsed__card__available_balance = float(parsed__card__available_balance.replace(",", ""))
                        parsed__card = {
                            "currency": parsed__card__currency,
                            "detail": parsed__card__detail,
                            "name": parsed__card__name,
                            "titular": parsed__card__is_titular,
                            "number": parsed__card__number,
                            "credit": {
                                "limit": parsed__card__limit_balance,
                                "available": parsed__card__available_balance,
                            }
                        }
                        parsed__cards.append(parsed__card)
                except KeyError: # first row has titles
                    pass
            parsed.append({
                "type": parsed__card__type,
                "accounts": parsed__cards,
            })
        return parsed
