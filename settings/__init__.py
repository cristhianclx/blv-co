# -*- coding: utf-8 -*-


MOCK_DIR = ".mocks"

DATABASE = {
    'db': 'data',
    'host': 'database',
    'port': 27017
}

BROWSER = 'http://browser:4444/wd/hub'

SCRAPER__CONFIGURATION = {
    'login': "https://bancaporinternet.bbva.pe/principal.html",
    'logout': "https://bancaporinternet.bbva.pe/bdntux_pe_web/bdntux_pe_web/shared/sesion/cerrar",
    'accounts': "https://bancaporinternet.bbva.pe/bdntux_pe_web/bdntux_pe_web/home/cuentas/index",
    'cards': "https://bancaporinternet.bbva.pe/bdntux_pe_web/bdntux_pe_web/home/tarjeta/index",
}


try:
    from settings.local import *
except ImportError:
    pass

try:
    from settings.testing import *
except ImportError:
    pass
