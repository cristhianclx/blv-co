# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from settings import BROWSER
from settings import SCRAPER__CONFIGURATION


class Scraper:

    def __init__(self, credentials):

        self.driver = webdriver.Remote(
            command_executor = BROWSER,
            desired_capabilities = DesiredCapabilities.FIREFOX
        )
        self.credentials = credentials

    def run(self):

        data = {}
        browser = self.driver
        # login
        browser.get(SCRAPER__CONFIGURATION['login'])
        try:
            browser_login_button_wait = WebDriverWait(browser, 30).until(
                EC.presence_of_element_located((By.ID, "btnEntrar"))
            )
        except:
            browser.quit()
        browser_login_document_type = browser.find_element_by_id("tipo-documento")
        browser_login_document_type_options = browser_login_document_type.find_elements_by_tag_name("option")
        for browser_login_document_type_option in browser_login_document_type_options:
            if browser_login_document_type_option.get_attribute("value") == self.credentials["document__type"]:
                browser_login_document_type_option.click()
        browser_login_document_number = browser.find_element_by_id("txteai_user")
        browser_login_document_number.send_keys('{}'.format(self.credentials["document__number"]))
        browser_login_password = browser.find_element_by_id("txteai_password")
        browser_login_password.send_keys('{}'.format(self.credentials["password"]))
        browser_login_button = browser.find_element_by_id("btnEntrar")
        browser_login_button.click()
        # homepage
        browser_window_after = browser.window_handles[1]
        browser.switch_to.window(browser_window_after)
        browser_homepage_logo = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.ID, "logo"))
        )
        # accounts
        browser.get(SCRAPER__CONFIGURATION['accounts'])
        browser_accounts_logo = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.ID, "logo"))
        )
        data["accounts"] = browser.page_source
        # cards
        browser.get(SCRAPER__CONFIGURATION['cards'])
        browser_cards_logo = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.ID, "logo"))
        )
        data["cards"] = browser.page_source
        # logout
        browser.get(SCRAPER__CONFIGURATION['logout'])
        browser_logout_logo = WebDriverWait(browser, 30).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "a.header__logo__link"))
        )
        browser.quit()
        return data
