#!/bin/bash

sudo apt-get update -y
sudo apt-get install -y \
  curl \
  jq

while ! curl -s "http://browser:4444/wd/hub/status" | jq '.status' | grep "0"; do
  echo 'browser ... waiting'
  sleep 1
done
echo "browser is ready"
