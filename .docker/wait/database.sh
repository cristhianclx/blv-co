#!/bin/bash

sudo apt-get update -y
sudo apt-get install -y \
  netcat-openbsd

until nc -z database 27017
do
  echo 'database ... waiting'
  sleep 1
done
echo "database is ready"
