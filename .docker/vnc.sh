#!/bin/bash

# need to install something to connect
if [ ! $(which "vinagre") ] ; then
   echo "Needed vinagre to connect to VNC"
fi

# allow to see what's running in browse
PORT_RAW=$(docker port browser 5900)
PORT=${PORT_RAW:8}
vinagre 127.0.0.1::$PORT
