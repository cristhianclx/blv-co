#!/bin/bash

py.test -v \
  --cov --no-cov-on-fail \
  -n 4
