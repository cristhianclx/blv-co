#!/bin/bash

gunicorn main:app \
  --reload \
  --bind 0.0.0.0:$PORT \
  --name web \
  --timeout 300 \
  --limit-request-line 0 \
  --max-requests 1000 \
  --worker-class gevent \
  --log-level debug
