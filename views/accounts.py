# -*- coding: utf-8 -*-

from falcon import HTTP_200
from falcon import HTTP_201
from falcon import HTTP_204
from falcon import HTTP_404
from falcon import HTTP_500
from falcon import MEDIA_TEXT

from datetime import datetime
from json import dumps as json__dumps
from mongoengine import errors
from mongoengine import queryset

from models import Account
from models import AccountData
from parser import Parser
from scraper import Scraper


class AccountsView:

    def on_post(self, request, response):

        try:
            data = request.media
            key = data['key']
            new = Account.objects.create(
                key = key,
                credentials = data["credentials"]
            )
            context = {
                "id": str(new.id),
                "key": new.key,
            }
            response.body = json__dumps(context)
            response.status = HTTP_201
        except errors.NotUniqueError:
            response.body = "Not unique"
            response.status = HTTP_500
            response.content_type = MEDIA_TEXT


class AccountsIDView:

    def on_get(self, request, response, key):

        try:
            item = Account.objects.get(key = key)
            context = item.json()
            response.body = json__dumps(context)
            response.status = HTTP_200
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT

    def on_put(self, request, response, key):

        data = request.media
        try:
            item = Account.objects.get(key = key)
            item.update(
                credentials = data,
                updated = datetime.utcnow()
            )
            context = {
                "id": str(item.id),
                "key": item.key,
            }
            response.body = json__dumps(context)
            response.status = HTTP_200
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT

    def on_delete(self, request, response, key):

        try:
            item = Account.objects.get(key = key)
            item.delete()
            response.status = HTTP_204
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT


class AccountsRunView:

    def on_get(self, request, response, key):

        try:
            item = Account.objects.get(key = key)
            scraper = Scraper(item.credentials)
            raw = scraper.run()
            new = AccountData.objects.create(
                account = item,
                raw = raw
            )
            new.save()
            parsed = Parser(new.raw)
            new.data = parsed.data()
            new.save()
            context = new.json()
            response.body = json__dumps(context)
            response.status = HTTP_201
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT
