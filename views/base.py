# -*- coding: utf-8 -*-

from falcon import MEDIA_TEXT

from jinja2 import Environment, FileSystemLoader

from utils import get__v


falcon_template = Environment(loader = FileSystemLoader('templates'))


class IndexView:

    def on_get(self, request, response):

        template = falcon_template.get_template('index.txt')
        context = {
        }
        response.body = template.render(context)
        response.content_type = MEDIA_TEXT


class PINGView:

    def on_get(self, request, response):

        template = falcon_template.get_template('ping.json')
        context = {
            'v': get__v()
        } 
        response.body = template.render(context)


class RobotsView:

    def on_get(self, request, response):

        template = falcon_template.get_template('robots.txt')
        context = {
        }
        response.body = template.render(context)
        response.content_type = MEDIA_TEXT
