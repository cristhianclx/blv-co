# -*- coding: utf-8 -*-

from falcon import HTTP_200
from falcon import HTTP_404
from falcon import MEDIA_TEXT

from json import dumps as json__dumps
from mongoengine import queryset

from models import Account
from models import AccountData


class AccountsDataView:

    def on_get(self, request, response, key):

        try:
            account = Account.objects.get(key = key)
            items = []
            items__raw = AccountData.objects.filter(account = account).order_by("-date")
            for item__raw in items__raw:
                items.append(item__raw.json())
            response.body = json__dumps(items)
            response.status = HTTP_200
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT


class AccountsDataLatestView:

    def on_get(self, request, response, key):

        try:
            account = Account.objects.get(key = key)
            items = []
            items__raw = AccountData.objects.filter(account = account).order_by("-date")
            if len(items__raw) >= 1:
                context = items__raw[0].json()
                response.status = HTTP_200
            else:
                context = {}
                response.status = HTTP_404
            response.body = json__dumps(context)
        except queryset.DoesNotExist:
            response.body = "Not found"
            response.status = HTTP_404
            response.content_type = MEDIA_TEXT
