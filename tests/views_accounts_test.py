# -*- coding: utf-8 -*-

from falcon import HTTP_CREATED
from falcon import HTTP_INTERNAL_SERVER_ERROR
from falcon import HTTP_NO_CONTENT
from falcon import HTTP_NOT_FOUND
from falcon import HTTP_OK
from falcon import testing

from mock.scraper import Mock as ScraperToMock
from pytest import fixture as pytest__fixture
from pytest import mark as pytest__mark
from unittest.mock import MagicMock

from main import app
from parser import Parser
from scraper import Scraper


URL_ACCOUNTS = '/accounts/'
URL_ACCOUNTS_ITEM = '/accounts/{}/'
URL_ACCOUNTS_RUN = '/accounts/{}/run/'


@ScraperToMock
class ScraperMock(Scraper):

    pass


@pytest__fixture
def api():

    return testing.TestClient(app)


def test__views_accounts__new(api):

    data = {
        "key": "cristhian-new@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "12345678",
            "password": "87654321",
        }
    }
    new__response = api.simulate_post('{}'.format(URL_ACCOUNTS), json = data)
    assert new__response.status == HTTP_CREATED
    new__response__data = new__response.json
    assert "id" in new__response__data
    assert len(new__response__data["id"]) == 24
    assert new__response__data["key"] == data["key"]

    new__response = api.simulate_post('{}'.format(URL_ACCOUNTS), json = data)
    assert new__response.status == HTTP_INTERNAL_SERVER_ERROR


@pytest__fixture
def new__account(api):

    data = { # we are trying to mock this
        "key": "cristhianclx@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "70204686",
            "password": "123456",
        }
    }
    new__response = api.simulate_post(URL_ACCOUNTS, json = data)
    assert new__response.status == HTTP_CREATED
    return data


def test__views_accounts(api, new__account):

    details__response = api.simulate_get(URL_ACCOUNTS_ITEM.format(new__account["key"]))
    assert details__response.status == HTTP_OK
    details__response__data = details__response.json
    assert "id" in details__response__data
    assert "credentials" in details__response__data
    assert new__account["key"] == details__response__data["key"]

    details__response__404 = api.simulate_get(URL_ACCOUNTS_ITEM.format("user-not-found"))
    assert details__response__404.status == HTTP_NOT_FOUND

    change__response = api.simulate_put(URL_ACCOUNTS_ITEM.format(new__account["key"]), json = {})
    assert change__response.status == HTTP_OK
    change__response__data = change__response.json
    assert "id" in change__response__data
    assert new__account["key"] == change__response__data["key"]

    details__response__modified = api.simulate_get(URL_ACCOUNTS_ITEM.format(new__account["key"]))
    assert details__response__modified.status == HTTP_OK
    details__response__modified__data = details__response__modified.json
    assert {} == details__response__modified__data["credentials"]

    change__response__404 = api.simulate_put(URL_ACCOUNTS_ITEM.format("user-not-found"))
    assert change__response__404.status == HTTP_NOT_FOUND

    delete__response = api.simulate_delete(URL_ACCOUNTS_ITEM.format(new__account["key"]))
    assert delete__response.status == HTTP_NO_CONTENT

    delete__response__deleted = api.simulate_get(URL_ACCOUNTS_ITEM.format(new__account["key"]))
    assert delete__response__deleted.status == HTTP_NOT_FOUND

    delete__response__404 = api.simulate_delete(URL_ACCOUNTS_ITEM.format("user-not-found"))
    assert delete__response__404.status == HTTP_NOT_FOUND


def test__views_accounts__run__new(monkeypatch, api):

    data = {
        "key": "dev@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "12345678",
            "password": "87654321",
        }
    }
    new__response = api.simulate_post('{}'.format(URL_ACCOUNTS), json = data)
    assert new__response.status == HTTP_CREATED

    monkeypatch.setattr(Scraper, "__init__", MagicMock(return_value = None))
    monkeypatch.setattr(Scraper, "run", MagicMock(return_value = {}))

    monkeypatch.setattr(Parser, "_accounts", MagicMock(return_value = {}))
    monkeypatch.setattr(Parser, "_cards", MagicMock(return_value = {}))

    run__response = api.simulate_get(URL_ACCOUNTS_RUN.format(data["key"]))
    assert run__response.status == HTTP_CREATED
    run__response__data = run__response.json
    assert "id" in run__response__data
    assert data["key"] == run__response__data["account__key"]
    run__response__data__details = {
      "accounts": {
      },
      "cards": {
      },
    }
    assert run__response__data__details == run__response__data["data"]


def test__views_accounts__run(monkeypatch, api, new__account):

    run__response__404 = api.simulate_get(URL_ACCOUNTS_RUN.format("user-not-found"))
    assert run__response__404.status == HTTP_NOT_FOUND

    monkeypatch.setattr(Scraper, "__init__", ScraperMock.__init__)
    monkeypatch.setattr(Scraper, "run", ScraperMock.run)

    run__response = api.simulate_get(URL_ACCOUNTS_RUN.format(new__account["key"]))
    assert run__response.status == HTTP_CREATED
    run__response__data = run__response.json
    assert "id" in run__response__data
    assert new__account["key"] == run__response__data["account__key"]
    run__response__data__accounts = [{
        "type": "Compensación por Tiempo de Servicio (CTS)",
        "accounts": [{
            "currency": "PEN",
            "account_number": "0011-0237-0710010373",
            "balance": {
                "countable": 444.12,
                "available": 444.12
            }
        }]
    }]
    assert run__response__data__accounts == run__response__data["data"]["accounts"]
    run__response__data__cards = [{
        'type': 'Tarjetas',
        'accounts': [{
            'currency': 'PEN',
            'detail': 'tarjeta-credito',
            'name': 'BBVA BFREE',
            'titular': True,
            'number': '4919-0982-6048-1664',
            'credit': {
                'limit': 2200.0,
                'available': 1403.93
            }
        }]
    }]
    assert run__response__data__cards == run__response__data["data"]["cards"]
