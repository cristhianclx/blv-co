# -*- coding: utf-8 -*-

from falcon import HTTP_OK
from falcon import testing

from git import Repo
from pytest import fixture as pytest__fixture

from main import app


@pytest__fixture
def api():

    return testing.TestClient(app)


def test__views_base__index(api):

    index__response__data = "¯\\_(ツ)_/¯"
    index__response = api.simulate_get('/')
    assert index__response.status == HTTP_OK
    assert index__response.text == index__response__data


def test__views_base__ping(monkeypatch, api):

    monkeypatch.setenv("v", "COMMIT_ID_SHA")

    ping__response__data = {
        "message": "pong",
        "v": "COMMIT_ID_SHA"
    }
    ping__response = api.simulate_get('/ping')
    assert ping__response.status == HTTP_OK
    assert ping__response.json == ping__response__data

    monkeypatch.delenv("v")
    monkeypatch.setattr(Repo, "head", None)
     
    ping__response__data = {
        "message": "pong",
        "v": "?"
    }
    ping__response = api.simulate_get('/ping')
    assert ping__response.status == HTTP_OK
    assert ping__response.json == ping__response__data


def test__views_base__robots(api):

    robots__response__data = "User-agent: *\nDisallow: /"
    robots__response = api.simulate_get('/robots.txt')
    assert robots__response.status == HTTP_OK
    assert robots__response.text == robots__response__data
