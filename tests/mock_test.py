# -*- coding: utf-8 -*-

from datetime import datetime
from mock.utils import mock__read
from mock.utils import mock__write


def test_mock_utils(monkeypatch):

    import settings
    monkeypatch.setattr(settings, 'MOCK_DIR', '/tmp')

    filename = "mock-{}".format(datetime.utcnow().strftime("%s"))

    key = {
        "key": "cristhianclx@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "70204686",
            "password": "12345678",
        }
    }
    assert mock__read(filename, key) == {}

    data = {
        "accounts": "RAW-DATA",
        "cards": "RAW-DATA",
    }
    mock__write(filename, key, data)
    assert mock__read(filename, key) == data

    assert mock__read(filename, "key-not-found") == {}
