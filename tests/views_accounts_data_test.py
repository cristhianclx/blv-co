# -*- coding: utf-8 -*-

from falcon import HTTP_CREATED
from falcon import HTTP_NOT_FOUND
from falcon import HTTP_OK
from falcon import testing

from pytest import fixture as pytest__fixture
from time import sleep

from main import app
from scraper import Scraper
from tests.views_accounts_test import ScraperMock
from tests.views_accounts_test import URL_ACCOUNTS
from tests.views_accounts_test import URL_ACCOUNTS_RUN


URL_ACCOUNTS_DATA = '/accounts/{}/data/'
URL_ACCOUNTS_DATA_LATEST = '/accounts/{}/data/latest/'


@pytest__fixture
def api():

    return testing.TestClient(app)


@pytest__fixture
def new__account(api):

    data = { # we are trying to mock this
        "key": "cristhian-data@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "70204686",
            "password": "123456",
        }
    }
    new__response = api.simulate_post(URL_ACCOUNTS, json = data)
    assert new__response.status == HTTP_CREATED

    data__no__run = {
        "key": "cristhian-data-no-run@gmail.com",
        "credentials": {
            "document__type": "L",
            "document__number": "70204686",
            "password": "123456",
        }
    }
    new__response = api.simulate_post(URL_ACCOUNTS, json = data__no__run)
    assert new__response.status == HTTP_CREATED

    return data, data__no__run


@pytest__fixture
def new__account__run(monkeypatch, api):

    monkeypatch.setattr(Scraper, "__init__", ScraperMock.__init__)
    monkeypatch.setattr(Scraper, "run", ScraperMock.run)

    run__response = api.simulate_get(URL_ACCOUNTS_RUN.format("cristhian-data@gmail.com"))
    assert run__response.status == HTTP_CREATED

    sleep(1) # to allow date to change
    run__response = api.simulate_get(URL_ACCOUNTS_RUN.format("cristhian-data@gmail.com"))
    assert run__response.status == HTTP_CREATED

    run__response__data = run__response.json
    return run__response__data


def test__views_accounts__data(api, new__account, new__account__run):

    data__response__404 = api.simulate_get(URL_ACCOUNTS_DATA.format("user-not-found"))
    assert data__response__404.status == HTTP_NOT_FOUND

    data__response = api.simulate_get(URL_ACCOUNTS_DATA.format(new__account[0]["key"]))
    assert data__response.status == HTTP_OK
    data__response__data = data__response.json
    assert len(data__response__data) == 2
    data__response__data = data__response__data[0]
    assert "id" in data__response__data
    assert new__account[0]["key"] == data__response__data["account__key"]
    data__response__data__data = {
        "accounts": [{
            "type": "Compensación por Tiempo de Servicio (CTS)",
            "accounts": [{
                "currency": "PEN",
                "account_number": "0011-0237-0710010373",
                "balance": {
                    "countable": 444.12,
                    "available": 444.12
                }
            }]
        }],
        "cards": [{
            'type': 'Tarjetas',
            'accounts': [{
                'currency': 'PEN',
                'detail': 'tarjeta-credito',
                'name': 'BBVA BFREE',
                'titular': True,
                'number': '4919-0982-6048-1664',
                'credit': {
                    'limit': 2200.0,
                    'available': 1403.93
                }
            }]
        }]
    }
    assert data__response__data__data == data__response__data["data"]

    data_latest__response__404 = api.simulate_get(URL_ACCOUNTS_DATA_LATEST.format("user-not-found"))
    assert data_latest__response__404.status == HTTP_NOT_FOUND

    data_latest__response__no_results = api.simulate_get(URL_ACCOUNTS_DATA_LATEST.format(new__account[1]["key"]))
    assert data_latest__response__no_results.status == HTTP_NOT_FOUND
    data_latest__response__no_results__data = data_latest__response__no_results.json
    assert data_latest__response__no_results__data == {}

    data_latest__response = api.simulate_get(URL_ACCOUNTS_DATA_LATEST.format(new__account[0]["key"]))
    assert data_latest__response.status == HTTP_OK
    data_latest__response__data = data_latest__response.json
    assert "id" in data_latest__response__data
    assert new__account[0]["key"] == data_latest__response__data["account__key"]
    assert new__account__run["date"] == data_latest__response__data["date"]
    data_latest__response__data__data = {
        "accounts": [{
            "type": "Compensación por Tiempo de Servicio (CTS)",
            "accounts": [{
                "currency": "PEN",
                "account_number": "0011-0237-0710010373",
                "balance": {
                    "countable": 444.12,
                    "available": 444.12
                }
            }]
        }],
        "cards": [{
            'type': 'Tarjetas',
            'accounts': [{
                'currency': 'PEN',
                'detail': 'tarjeta-credito',
                'name': 'BBVA BFREE',
                'titular': True,
                'number': '4919-0982-6048-1664',
                'credit': {
                    'limit': 2200.0,
                    'available': 1403.93
                }
            }]
        }]
    }
    assert data_latest__response__data__data == data_latest__response__data["data"]
