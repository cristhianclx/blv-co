# -*- coding: utf-8 -*-

from codecs import decode as codecs__decode
from codecs import encode as codecs__encode
from git import Repo
from os import getenv
from mongoengine import connect as mongo__connect
from pickle import dumps as pickle__dumps
from pickle import loads as pickle__loads

from settings import DATABASE


def connect__to__database():

    mongo__connect(**DATABASE)


def get__v():

    v = getenv("v")
    if v == None:
        try:
            repo = Repo('.', search_parent_directories=True)
            return repo.head.object.hexsha[:7]
        except:
            return "?"
    else:
        return v


def dumps(data):

    return codecs__encode(pickle__dumps(data), "base64").decode()


def loads(data):

    return pickle__loads(codecs__decode(data.encode(), "base64"))
