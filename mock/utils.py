# -*- coding: utf-8 -*-

from json import dumps as json__dumps
from json import loads as json__loads

from settings import MOCK_DIR
from utils import dumps
from utils import loads


def mock__read(filename, key):

    key = dumps(key)
    with open("{}/{}.json".format(MOCK_DIR, filename), 'a+') as mock_file:
        mock_file.seek(0)
        try:
            mock_data = json__loads(mock_file.read())
            if key in mock_data:
                data = mock_data[key]
                return loads(data)
            return {}
        except:
            return {}


def mock__write(filename, key, data):

    key = dumps(key)
    data = dumps(data)
    with open("{}/{}.json".format(MOCK_DIR, filename), 'w') as mock_file:
        mock_file.seek(0)
        try:
            mock_data = json__loads(mock_file.read())
        except:
            mock_data = {}
        mock_data[key] = data
        mock_file.write(json__dumps(mock_data, indent = 2, sort_keys = True))
