# -*- coding: utf-8 -*-

from mock.utils import mock__read
from mock.utils import mock__write


FILENAME = 'scrape'


def Mock(cls):

    # __init__

    __init__ = getattr(cls, '__init__')

    def new____init__(self, credentials):

        self.filename = FILENAME
        self.key = dict(sorted(credentials.items()))
        if mock__read(self.filename, self.key) == {}:
            __init__(self, credentials)

    setattr(cls, '__init__', new____init__)

    # run

    run = getattr(cls, 'run')

    def new__run(self):

        if mock__read(self.filename, self.key) == {}:
            data = run(self)
            mock__write(self.filename, self.key, data)
            return data
        else:
            data = mock__read(self.filename, self.key)
            return data

    setattr(cls, 'run', new__run)

    return cls
