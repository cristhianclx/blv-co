# -*- coding: utf-8 -*-

from datetime import datetime
from mongoengine import *


class Account(Document):

    key = EmailField(
        required = True,
        max_length = 150,
        unique = True
    )
    credentials = DictField(
        required = True
    )
    created = DateTimeField(
        default = datetime.utcnow
    )
    updated = DateTimeField(
        default = datetime.utcnow
    )

    meta = {
        'ordering': ['-updated']
    }

    def json(self):

        return {
            'id': str(self.id),
            'key': self.key,
            'credentials': self.credentials,
            'created': self.created.strftime('%s'),
            'updated': self.updated.strftime('%s'),
        }


class AccountData(Document):

    account = ReferenceField(Account,
        required = True,
        reverse_delete_rule = CASCADE
    )
    raw = DictField()
    data = DictField()
    date = DateTimeField(
        default = datetime.utcnow
    )

    meta = {
        'ordering': ['-date']
    }

    def json(self):

        return {
            'id': str(self.id),
            'account__key': self.account.key,
            'data': self.data,
            'date': self.date.strftime('%s'),
        }
