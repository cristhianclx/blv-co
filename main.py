# -*- coding: utf-8 -*-

from falcon import API

from utils import connect__to__database
from views.base import IndexView, PINGView, RobotsView
from views.accounts import AccountsView, AccountsIDView, AccountsRunView
from views.accounts__data import AccountsDataView, AccountsDataLatestView


connect__to__database()


app = API()
app.req_options.strip_url_path_trailing_slash = True

app.add_route('/'          , IndexView())
app.add_route('/ping/'     , PINGView())
app.add_route('/robots.txt', RobotsView())

app.add_route('/accounts/'          , AccountsView())
app.add_route('/accounts/{key}/'    , AccountsIDView())
app.add_route('/accounts/{key}/run/', AccountsRunView())

app.add_route('/accounts/{key}/data/'       , AccountsDataView())
app.add_route('/accounts/{key}/data/latest/', AccountsDataLatestView())
