# -*- coding: utf-8 -*-

from falcon import __package__ as running__package
from falcon import __version__ as running__version

from datetime import datetime
from ipaddress import ip_address
from sys import argv as sys__argv
from wsgiref.simple_server import make_server
from wsgiref.validate import validator

from main import app


if __name__ == '__main__': # only needed to debug

    print("Performing system checks ...")
    run = validator(app)
    print("System check identified no issues.")
    print("{}".format(datetime.now().strftime("%B %-d, %Y - %H:%M:%S ")))
    print("{} version {}".format(running__package, running__version))
    DEFAULT_HOST = "127.0.0.1"
    DEFAULT_PORT = 8000
    HOST_AND_PORT_RAW = "{}:{}".format(DEFAULT_HOST, DEFAULT_PORT)
    if len(sys__argv) > 1:
        HOST_AND_PORT_RAW = sys__argv[1]
    try:
        HOST_AND_PORT = HOST_AND_PORT_RAW.split(":")
        if len(HOST_AND_PORT) == 1:
            HOST = DEFAULT_HOST
            PORT = int(HOST_AND_PORT[0])
        elif len(HOST_AND_PORT) == 2:
            HOST = HOST_AND_PORT[0]
            if HOST == "0":
                HOST = "0.0.0.0"
            elif HOST == "localhost":
                HOST = DEFAULT_HOST
            else:
                ip_address(HOST)
            PORT = int(HOST_AND_PORT[1])
        else:
            print("warning: {}".format("Not appears to be something like 127.0.0.1:8000"))
            HOST = DEFAULT_HOST
            PORT = DEFAULT_PORT
    except Exception as e:
        print("warning: {}".format(e))
        HOST = DEFAULT_HOST
        PORT = DEFAULT_PORT
    with make_server(HOST, PORT, run) as httpd:
        try:
            print("Starting development server at http://{}:{}/".format(HOST, PORT))
            print("Quit the server with CONTROL-C.")
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("Shutting down.")
            httpd.server_close()
