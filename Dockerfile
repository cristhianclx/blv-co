FROM python:3.8-slim-buster

ARG DEBIAN_FRONTEND=noninteractive

ENV USERNAME="dev"
ENV PASSWORD="secret"

ENV DIRECTORY="/code"
ENV PATH="${PATH}:$DIRECTORY/.local/bin"

RUN \
  DEPENDENCIES=' \
    git \
    gcc \
    libc6-dev \
    libpcre3 \
    libssl-dev \
    make \
    sudo \
    vim \
  ' \
  && apt-get update -y \
  && apt-get install --no-install-recommends -y $DEPENDENCIES \
  && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /usr/share/locale

RUN \
  mkdir -p $DIRECTORY \
  && useradd $USERNAME --shell /bin/bash --home-dir $DIRECTORY \
  && chown -R $USERNAME:$USERNAME $DIRECTORY \
  && echo "$USERNAME:$PASSWORD" | chpasswd \
  && usermod -a -G sudo $USERNAME \
  && echo "$USERNAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

RUN \
  echo "set mouse-=a" >> /root/.vimrc \
  && echo "set mouse-=a" >> $DIRECTORY/.vimrc

USER $USERNAME

COPY --chown=$USERNAME:$USERNAME ./requirements.txt /tmp/requirements.txt

RUN \
  pip install --upgrade pip \
  && pip install --no-cache-dir Cython==0.29.16 \
  && pip install --no-cache-dir -r /tmp/requirements.txt

WORKDIR $DIRECTORY
COPY --chown=$USERNAME:$USERNAME . $DIRECTORY

RUN chmod +x $DIRECTORY/.docker/*.sh
CMD $DIRECTORY/.docker/run.sh
